# NeTeGe

**This repository is a republished version of the one used by the general students council of the Emmy Noether University Göttingen in 2020. Some information here is out of date!**

*This project will be merged into a different project and will be deleted. Everything in here is experimental. Use with caution. The code still contains many bugs.*

NeTeGe (pronunciation: eNTe Geh) is a html-mail newsletter template generator written in bash and python.

*todo badges and logo*

Features of NeTeGe are
* generate valid html that most mail clients support
* inline images
* multiple language supprt
* preface, texts and events
* generate law foo from config
and much more.

The tool is written to be useful for the [Allgemeiner Studierenden Ausschuss (AStA)](https://asta.uni-goettingen.de/) of the [University of Göttingen](https://uni-goettingen.de/).

## Table of Content

0. [Table of Content](#table-of-content)
1. [Installatin](#installation)
1. [Usage](#Usage)
1. [Configuration](#configuration)
1. [Customisation](#customisation)
1. [Documentation](#documentation)
1. [Future Versions](#future-versions)
1. [Licencse](#licencse)

## Intallation

To use this software you can simply download this repository and start main.py if the following raquirements are satisfied.

```
git clone git@gitlab.gwdg.de:felix.schelle1/netege.git
```

Future versions will be available as a .tar archive.

### Requirements

* python 3.6+
    * tk (if you want to use the GUI (which does not exist yet))
* bash
    * wget
    * sed
    * convert (imagemagick)

## Usage

just `./gen.py` and adjust the code in the file before executing.  

In the future probably something like:

```
main.py [-h] [-c CONFIGFILE] [-g] [-v]
```

### Steps to get a newsletter.html

1. clone this repo
1. fix syntax in your newletter pad
1. double-check all the syntax in your newsletter pad. this usually takes about an hour
1. bring the pad in special form:
    * remove everythin that must not be in the published newsletter (e.g. "@asterix hier noch $etwas einfügen")
    * every text title (english and german) is written with `##`
    * events are titled with `## Events` or `## Termine`
    * Each german text is instantly followed by its english counterpart, including the events
    * preface is handled the same as the texts
    * links must be in md-syntax and complete correct (starting with `https://` or if mail with `mailto:`)
    * events must be written as a table with 3 coloumns in excact order: date, location, title (check last newsletter for format)
1. adjust the main-function at the bottom lines of gen.py (hard coded padlink and month name etc)
1. call gen.py and read output (you will get a messages that the images are not handled)
1. `cp newsletter.html.tpl newsletter.html` and open it in a browser to early check for errors (events table styling)
1. open the generated newsletter.html.tpl and handle the images manually. you should ask asterix for details. and adjust the event tables (german and english) now if necessary.
1. get all the image links and save them in a file (`grep img newsletter.html.tpl > wgeturls` and then delete in each line chars that are not part of the link)
1. dowload all the images (`wget -i wgeturls`)
1. replace image placeholders with base64-encoded inline images using the script `imgwget2b64sed.sh` (hard coded filenames inside)
1. now open newsletter.html and check for errors (wrong links, wrong month, english and german mixed, wrong parsed events)
1. disable image loading to check if image alt texts are correct (or read in the sourcecode)
1. file is done. do the other needed stuff now
    1. upload the newsletter and link it on the newsletter webpage
    1. login with the öffentlichkeitsarbeit funktionsaccount in your mailclient (thunderbird work fine)
    1. copy full content of the final html file (`xclip -sel c < newsletter.html`)
    1. paste html content in new mail (use the html-paste function, not simply paste)
    1. allow html mail sending (is enabled by default)
    1. send mail from correct mailadress to correct mailadress. ask asterix for help if doing the first time

## Configuration

*not implemented yet*

You probably want to do some configurations before you use this tool.
All configuration is done in csv-files.

*todo explain configurations*

## Customisation

NeTeGe will be written modular in the future supporting different newsletter themes.

## Documentation

The technical documentation of this software will be available online.
*todo write documention*

## Future Versions

* [ ] Multilanguage support
* [ ] Web-GUI (NeTeGe as a service)

## Licencse

*todo write proper license foo*

Beerware, AGPL
