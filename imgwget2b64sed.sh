#!/bin/sh

set -euo pipefail

tpl="newsletter.html.tpl"       # source file
newsletter="newsletter.html"    # target file
ascfile="tmp.asc"               # temporary file containing base64 image data

cp "$tpl" "$newsletter"

# todo check if TEXT files are valid (does not contain %,&,~ and other bad characters)

patternold="https://pad.gwdg.de/uploads/"
#upload_fa0aa31133073236781bf2853c1b7c09.png"
patternnew="data:image/png;base64,"

# todo add grep urls and wget -i urlsfile

for pngfile in upload*;
do
    echo "Handle $pngfile"
    # todo check of size is 200x200, because convert -resize fails if the size is already correct
    convert -resize 200x200 $pngfile tmp.png
    convert tmp.png PNG:- | base64 > tmp
    tr -d '\n' < tmp > $ascfile

    sed -i -f - $newsletter << EOF
s%$(echo $patternold)$(echo $pngfile)%$(echo $patternnew)$(cat $ascfile)%g
EOF
    echo "inserting image $pngfile done"
done

echo "Finished inserting all images and announcments.\nCleaning up."
rm tmp*
