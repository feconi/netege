<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>AStA-Newsletter __date_de__</title>
</head>
<body class="body">
    <div class="basediv">
        <p class="basep">
            <a class="helpa" href="__publish_url__">
                Probleme bei der Darstellung? Hier klicken.<br>
                Problems with rendering? Click here.
            </a>
        </p>
