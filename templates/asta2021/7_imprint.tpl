        <div class="imdiv">
            <p class="imprint">
                <b>__imprint__</b>
            </p>
            <p class="imprint">
                __responsible__
                · __rapl__
            </p>
            <p class="imprint">
                __address__
                · <a class="a" href="__gdpr_url__">__gdpr__</a>
            </p>
            <p class="imprint">
                Tel: __telephone__
                · E-Mail: <a class="a" href="mailto:__mail__">__mail__</a>
                · Web: <a class="a" href="__web__">__web__</a>
            </p>
        </div>
