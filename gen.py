#!/usr/bin/env python3

import os
import json
import markdown

"""
def laod_texts(path="local/", number_of_events):
    texts = []
    return texts

def load_events():
    events = []
    return events
"""


def setup_tpl(texts, h, events=None, config_path="templates/asta2021/config.json"):
    # config is config of the year
    # config of the month is the quick dirty part below and not yet fully implemented

    with open(config_path, 'r') as f:
        config = json.load(f)
    # todo load more config , but actually give all config per parameter
    # __date_de__
    # __date_en__
    # todo give events

    # dirty quick
    languages = ('de', 'en')
    config['languages'] = languages
    config['date'] = {'de': "August 2021", 'en': "August 2021"}
    config['announcements'] = {'de': "Aktuelle Meldungen", 'en': "Current announcementes"}
    config['events'] = {'de': "Termine", 'en': "Events"}
    # end dirty quick

    def load_file(path):
        with open("templates/asta2021/"+path, 'r') as f:
            data = f.read()
        return data

    tpl = []

    tpl_part = load_file('0_base_start.tpl')
    tpl_part = tpl_part.replace('__date_de__', config['date']['de'])
    tpl_part = tpl_part.replace('__publish_url__', config['publish_url'])
    tpl.append(tpl_part)

    if len(config['languages']) > 1:
        tpl_part = load_file('1_jumpdown.tpl')
        tpl_part = tpl_part.replace('__below_anchor__', config['languages'][-1])    # todo build support for n > 2 languages
        tpl_part = tpl_part.replace('__below_text__', "ENGLISH VERSION BELOW")
        tpl.append(tpl_part)

    for language in config['languages']:

        tpl_part = load_file('2_preface.tpl')
        tpl_part = tpl_part.replace('__language__', language)
        tpl_part = tpl_part.replace('__date__', config['date'][language])
        tpl_part = tpl_part.replace('__preface__', texts[0][language])
        tpl.append(tpl_part)

        tpl_part = load_file('3_0_texts_start.tpl')
        tpl_part = tpl_part.replace('__announcements__', config['announcements'][language])
        tpl.append(tpl_part)

        tpl_part = load_file('3_1_text.tpl')
        eoswitch = ("even", "odd")
        for n in range(1, len(h)):
            tpl_part_n = tpl_part
            tpl_part_n = tpl_part_n.replace('__eoswitch__', eoswitch[n%2])
            tpl_part_n = tpl_part_n.replace('__image__', f"__image_{n}_{language}__") # this must change in the template files
            tpl_part_n = tpl_part_n.replace('__image_description__', f"__image_description_{n}_{language}__")
            tpl_part_n = tpl_part_n.replace('__h__', h[n][language])
            tpl_part_n = tpl_part_n.replace('__text__', texts[n][language])
            tpl.extend(tpl_part_n)

        tpl_part = load_file('3_2_texts_end.tpl')
        tpl.append(tpl_part)

        if events:
            tpl_part = load_file('6_0_events_start.tpl')
            tpl_part = tpl_part.replace('__events__', config['events'][language])
            tpl.append(tpl_part)

            tpl_part = load_file('6_1_event.tpl')

            for e in events:
                tpl_part_n = tpl_part
                # for idx, val in enumerate(tpl_part_n):
                tpl_part_n = tpl_part_n.replace('__event_date__', e['date'][language])
                tpl_part_n = tpl_part_n.replace('__event_location__', e['location'][language])
                tpl_part_n = tpl_part_n.replace('__event_name__', e['name'][language])
                tpl.append(tpl_part_n)

            tpl_part = load_file('6_2_events_end.tpl')
            tpl.append(tpl_part)

        tpl_part = load_file('7_imprint.tpl')
        tpl_part = tpl_part.replace('__imprint__', config['imprint'][language])
        tpl_part = tpl_part.replace('__responsible__', config['imprint']['responsible'][language])
        tpl_part = tpl_part.replace('__rapl__', config['imprint']['rapl'][language])
        tpl_part = tpl_part.replace('__address__', config['imprint']['address'])
        tpl_part = tpl_part.replace('__gdpr_url__', config['imprint']['gdpr_url'][language])
        tpl_part = tpl_part.replace('__gdpr__', config['imprint']['gdpr'][language])
        tpl_part = tpl_part.replace('__telephone__', config['imprint']['telephone'])
        tpl_part = tpl_part.replace('__mail__', config['imprint']['mail'])
        tpl_part = tpl_part.replace('__web__', config['imprint']['web'][language])
        tpl.append(tpl_part)

    tpl_part = load_file('8_base_end.tpl')
    tpl.append(tpl_part)

    return ''.join(tpl)

def download_pad(url, path="local/paddump.md"):
    if not url.endswith("/download"):
        url = url + "/download"

    import urllib.request
    urllib.request.urlretrieve(url, path)
    return

def parse_paddump(path="local/paddump.md"):
    """
    input: path to paddump file
    returns: triple: list of text and list of hs, where each index is dict with key-value pairs of language and text in that language and list of events
    """
    # todo logic for de/en
    # dirty quick assuming de,en,de,en,...

    with open(path, 'r') as f:
        paddump = f.read()

    texts = paddump.split("## ")[1:] # skip h1 including month
    # todo maybe bug here that ### gets splited and shouldnt
    #month = texts.pop(0)
    hs = []
    event_indicators = ('Events', 'Termine', 'Veranstaltungen')
    events = {}
    all_html = []

    de = True    # language switch
    events_language = 'de'  # events language switch
    for t in texts:
        # todo add ## to still existing # beacsuse ## was splitted
        h_end = t.index('\n')
        h = t[:h_end]
        if h in event_indicators:
            """
            with open("local/not_text.md", 'a+') as f:
                f.write(t)
            """
            events[events_language] = t
            events_language = 'en'  # de is done, next (if any) can only be en
            # todo support more than 2 languages
        else:
            if de:
                hs.append({'de': t[:h_end]})
            else:
                hs[-1]['en'] = t[:h_end]
            md = t[h_end:]
            html = markdown.markdown(md)

            # todo build more custom html, because html mail is not a standard
            html = html_replace(html, de=de)
            if de:
                all_html.append({'de': html})
            else:
                all_html[-1]['en'] = html
            de = not de
    return all_html, hs, events

def html_replace(html, in_events=False, de=True):
    """
    This function is only needed because mail html is not compliant with html.
    Therfore some tags and attributes must be changed
    """
    html = html.replace('strong>', 'b>')
    html = html.replace('em>', 'i>')
    html = html.replace('alt=""', f'alt="__placeholder_{de}__"')
    html = html.replace('<a ', '<a class="a" ') if not in_events else html.replace('<a ', '<a class="eva" ')
    return html

def parse_eventdump(events_raw):
    # todo languages 'de' and 'en' are assumed and below hard coded
    languages = events_raw.keys()
    events_html = []    # dict.fromkeys(languages)
    for language in languages:
        lines = events_raw[language].split("\n")
        tr_count = 0
        for line in lines:
            # todo trim line
            if line.startswith('|'):    # ignore non-table lines
                if tr_count > 1:    # 0 is th line, 1 is md-syntax line
                    if language == 'de':    # de is first language
                        events_html.append({'date': {}, 'location': {}, 'name': {}})
                    tds = line.split('|')[1:-1] # len() = 3 is expected for (date, location, name)
                    # todo trim all tds
                    tds_html = [markdown.markdown(td)[3:-4] for td in tds]  # [3:-4] for cutting <p>_</p>
                    for td_idx, val in enumerate(tds_html):
                        html_part = html_replace(val, in_events=True, de=(language=='de'))
                        KEYS = ('date', 'location', 'name')
                        events_html[tr_count-2][KEYS[td_idx]][language] = html_part
                tr_count += 1
    return events_html


def style_replace(tpl, path="templates/asta2021/style.css"):
    """
    input: prepared template, path to css file
    returns: more prepared template
    """

    def load_css(path):
        """
        input: path to css file
        returns: dict with key-value pairs containing css-class and associated styles
        """
        with open(path, 'r') as f:
            data = f.readlines()
        styles = {}
        for line in data:
            if line.startswith('.'):
                sel = line[1:line.index(" ")]
                styles[sel] = line[line.index("{")+1:line.index("}")]
        return styles

    styles = load_css(path)
    for selector, style in styles.items():
        tpl = tpl.replace(f'class="{selector}"', f'style="{style}"')
    return tpl

def main(padurl, template="templates/asta2021", output="newsletter.html.tpl"):
    # big todo clean file parameters
    # load config, still is hard coded

    print("Trying to download pad content and save as local/paddump.md")
    download_pad(url=padurl)
    print("done\n")

    print("Parsing pad content and converting each text to html")
    texts, hs, events_raw = parse_paddump()
    print("done\n")

    print("Parsing events from pad content and converting each event to html table")
    events = parse_eventdump(events_raw)
    print("done\n")

    print("loading template and inserting texts and event tables")
    tpl = setup_tpl(texts=texts, h=hs, events=events)
    print("done\n")
    print("The image are at the wrong position and maybe descriptions are missing. Check your file and correct each imgage in each text.\nThe image must be before the h3 of the text.\n")

    print("replacing style classes with inline styles (because html-mail sucks)")
    tpl = style_replace(tpl)
    print("done\n")

    print(f"Trying to save content to a file {output}")
    with open(output, "w+") as f:
        f.write(tpl)
    print("done\n")

    print("now left to do is wget images and base64 image convert using the sh-script (or doing it manually)")

if __name__ == "__main__":
    # todo argparse
    padurl = "https://pad.gwdg.de/HackyHourGoettingen"  # without ? or #
    # wrong: padurl = "https://pad.gwdg.de/HackyHourGoettingen?view#Upcoming-events"
    main(padurl=padurl)

